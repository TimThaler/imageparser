import time
import os
import argparse
import sys
from PIL import Image
from PIL.ExifTags import TAGS

def add_to_csv_file(fp, matrix):
    for i in range(len(matrix)):
        datapoint = ""
        for j in range(len(matrix[i])):
            datapoint += matrix[i][j] + ";"
        fp.write(datapoint + "\n")

def init_csv_file(path):
    CSV_HEADER = "Image_ID;Latitude;Longitude;Date_Time_Image;Model;Software"
    try:
        time_substring = time.strftime("%c").replace(" ", "_")
        postfix = path.split('/')[-1]
        file_name = "Parsed_" + postfix + "_" + time_substring + ".csv"
        fp = open(file_name, 'w')
        fp.write(CSV_HEADER)
        fp.write("\n")
        return fp
    except Exception as e:
        print("Failed to init csv file!")
        print("because: %s" %e)

def print_mul_dim_array(a):
    for i in range(len(a)):
        for j in range(len(a[i])):
            print(a[i][j])
        print()

def get_metadata(path):
    try:
        images = []
        # collect all images in array "images"
        for filename in os.listdir(path):
            if filename.endswith(".jpg"):
                images.append(filename)
            if filename.endswith(".JPG"):
                images.append(filename)

        image_info_matrix = [[0 for x in range(6)] for y in range(len(images)) ] 

        #init matrix with zeros
        for i in range(len(image_info_matrix)):
            for j in range(len(image_info_matrix[i])):
                image_info_matrix[i][j] = str(0)
    
        # open the collected images and collect desired tags
        for index, image_string in enumerate(images):
            image_info_matrix[index][0] = image_string
            im = Image.open(image_string);
            if im == None:
                print("Image not loaded")

            exif_data = im._getexif()
            if exif_data == None:
                print(" No data for image: " + image_string)
                continue

            exif_tag = {}  #type dict
            print("Image name: " + image_string)
            for key, value in exif_data.items():
                decoded_tag = TAGS.get(key,key)
                if decoded_tag == "GPSInfo":
                    print("\tFound GPS info:")
                    exif_tag[decoded_tag] = value
                    gps_info = {}
                    gps_info = exif_tag['GPSInfo']
                    lat = [float(x)/float(y) for x, y in gps_info[2]]
                    lon = [float(x)/float(y) for x, y in gps_info[4]]
                    lat = lat[0] + lat[1]/60 + lat[2]/3600
                    lon = lon[0] + lon[1]/60 + lon[2]/3600
                    latref = gps_info[1]
                    lonref = gps_info[3]
                    if latref == 'S':
                        lat = -lat
                    if lonref == 'W':
                        lon = -lon
    
                    image_info_matrix[index][1] = str(lat) 
                    image_info_matrix[index][2] = str(lon)
                if decoded_tag=="DateTimeOriginal":
                    image_info_matrix[index][3] = str(value)
                if decoded_tag=="Model":
                    image_info_matrix[index][4] = str(value)
                if decoded_tag=="Software":
                    image_info_matrix[index][5] = str(value)

        #print_mul_dim_array(image_info_matrix)
        csv_file = init_csv_file(path)
        add_to_csv_file(csv_file,image_info_matrix)
    except Exception as e:
         exc_type, exc_obj, exc_tb = sys.exc_info()
         fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
         print(exc_type, fname, exc_tb.tb_lineno)
    #    print("!Failed")
    #    print("because: %s" %e)

def Main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--path","-p", help="Path for folder with images")
    args = parser.parse_args()
    get_metadata(args.path)

if __name__ =='__main__':
    Main()
